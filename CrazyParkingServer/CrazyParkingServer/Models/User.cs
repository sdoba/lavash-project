﻿using System;
using System.Collections.Generic;

namespace CrazyParkingServer.Models
{
    public partial class User
    {
        public User()
        {
            Orders = new HashSet<Order>();
            Scores = new HashSet<Score>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; } = null!;
        public string Password { get; set; } = null!;
        public string? TelegramChatId { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Score> Scores { get; set; }
    }
}
