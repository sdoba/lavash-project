﻿using System;
using System.Collections.Generic;

namespace CrazyParkingServer.Models
{
    public partial class Telegrambot
    {
        public int BotId { get; set; }
        public string BotName { get; set; } = null!;
        public string ApiKey { get; set; } = null!;
    }
}
