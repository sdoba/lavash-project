﻿using System;
using System.Collections.Generic;

namespace CrazyParkingServer.Models
{
    public partial class Order
    {
        public int OrderId { get; set; }
        public DateTime Timestamp { get; set; }
        public int UserId { get; set; }
        public int ItemId { get; set; }

        public virtual Item Item { get; set; } = null!;
        public virtual User User { get; set; } = null!;
    }
}
