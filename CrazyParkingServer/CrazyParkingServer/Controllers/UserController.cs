﻿using CrazyParkingServer.Models;
using Microsoft.AspNetCore.Mvc;

namespace CrazyParkingServer.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly ILogger<UserController> _logger;

        public UserController(IUserRepository userRepository, ILogger<UserController> logger)
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        [HttpGet]
        [Route("/users")]
        public IActionResult GetUsers()
        {
            var users = _userRepository.users.ToList();
            return Ok(users);
        }
    }
}
