﻿using Microsoft.AspNetCore.Mvc;
using NotificationServer.Services;

namespace NotificationServer.Controllers
{
    public class NotificationController : Controller
    {
        readonly private ITelegramService _service;
        
        public NotificationController(ITelegramService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("/notification/telegram")]
        public IActionResult SendTelegramNotification(string apiKey, string chatId, string message)
        {
            _service.Send(apiKey, chatId, message);
            return Ok();
        }
    }
}
